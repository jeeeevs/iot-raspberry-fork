#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import sys

buzzer_gpio = 5
led_green_gpio = 18
led_red_gpio = 17
led_blue_gpio = 6

gpio_array = [buzzer_gpio,led_green_gpio,led_red_gpio, led_blue_gpio]

GPIO.setmode(GPIO.BCM)

for gpio in gpio_array:
    GPIO.setup(gpio,GPIO.OUT)

def beep(gpio, GPIO):
    GPIO.output(gpio, True)
    time.sleep(1)
    GPIO.output(gpio, False)
    time.sleep(1)

def two_beep(gpio, GPIO):
    beep(gpio, GPIO)
    beep(gpio, GPIO)

def light_on(color, GPIO):
    led_green_gpio = 18
    led_red_gpio = 17
    led_blue_gpio = 6
    buzzer_gpio = 5
    gpio = led_green_gpio;
    if color == 'red':
        gpio = led_red_gpio
    elif color == 'green':
        gpio = led_green_gpio
    elif color == 'blue':
        gpio = led_blue_gpio

    GPIO.output(gpio, True)
    if color == 'red':
        two_beep(buzzer_gpio, GPIO)
    elif color == 'green':
        beep(buzzer_gpio, GPIO)
    time.sleep(0.2)
    GPIO.output(gpio, False)


#two_beep(buzzer_gpio, GPIO)

color_required = sys.argv[1]
light_on(color_required, GPIO)
print color_required
GPIO.cleanup()
exit
