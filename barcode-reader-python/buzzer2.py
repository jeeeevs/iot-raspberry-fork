#!/usr/bin/python

import RPi.GPIO as GPIO
import time

gpio_port = 5
# Tell GPIO library to use GPIO references
GPIO.setmode(GPIO.BCM)

# Configure GPIO8 as an outout
GPIO.setup(gpio_port, GPIO.OUT)

# Turn Buzzer off
GPIO.output(gpio_port, False)

# Turn Buzzer on
GPIO.output(gpio_port, True)

# Wait 1 second
time.sleep(1)

 # Turn Buzzer off
# GPIO.output(gpio_port, False)
#
# # Wait 1 second
# time.sleep(1)
#
# # Turn Buzzer on
# GPIO.output(gpio_port, True)
#
# # Wait 1 second
# time.sleep(1)
#
# # Turn Buzzer off
# GPIO.output(gpio_port, False)

raw_input('The buzzer should have sounded twice, press enter to exit program')

# Reset GPIO settings
GPIO.cleanup()