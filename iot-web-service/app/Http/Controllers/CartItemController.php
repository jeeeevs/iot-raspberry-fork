<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CartItem;
use Symfony\Component\HttpFoundation\JsonResponse;

class CartItemController extends Controller
{
    public function addToCart(Request $request) {
        $content = $request->getContent();
        $cartDataArray = json_decode($content, true);
        $cartItem = new CartItem();
        $cartItem->cart_id = $cartDataArray['cart_id'];
        $cartItem->barcode = $cartDataArray['barcode'];
        $cartItem->save();
        return response($cartItem->toArray());
    }
    
    public function listItems() {
        return JsonResponse::create(CartItem::all()->toArray());
    }
    
    public function listItemsView() {
        $items = CartItem::all();    
        return view('items',compact('items'));
    }

    public function mobileView($cartId) {
        $items = CartItem::where('cart_id', $cartId)->get();
        return view('mobile',compact('items'));
    }
}
